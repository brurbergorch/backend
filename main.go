package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strings"
	"time"

	"gopkg.in/yaml.v2"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"gitlab.com/brurberg/log"
	"gitlab.com/brurbergorch/backend/helper"
	"gitlab.com/brurbergorch/backend/mail"
	"gitlab.com/brurbergorch/backend/state"
)

func loadEnvVar() {
	err := godotenv.Load()
	if err != nil {
		log.Println("Error loading .env file")
	}
}

type DB struct {
	DB *helper.DB
}

type Update struct {
	Key string `josn:"key"`
	Tag string `json:"tag"`
}

func main() {
	loadEnvVar()
	state.Debug = false
	mail.Send("sindre@brurberg.no", "BrurbergOrch Info: Main", "Service starting up")
	//time.Sleep(1 * time.Hour)
	//hosts := []string{"master", "192.168.2.212", "node2"}
	hosts := []string{"192.168.2.6", "192.168.2.212", "192.168.2.105"}
	err := getNewCerts(hosts)
	if err != nil {
		mail.Send("sindre@brurberg.no", "BrurbergOrch Error: get new cert", err.Error())
		log.Fatal(err)
	}
	quit := make(chan struct{})
	go func() {
		now := time.Now()
		if now.Hour() >= 3 {
			now = now.AddDate(0, 0, 1)
		}
		now = time.Date(now.Year(), now.Month(), now.Day(), 3, 0, 0, 0, now.Location())
		time.Sleep(time.Until(now))
		ticker := time.NewTicker(24 * time.Hour)
		for {
			select {
			case <-ticker.C:
				err := getNewCerts(hosts)
				if err != nil {
					mail.Send("sindre@brurberg.no", "BrurbergOrch Error: get new cert", err.Error())
					log.Fatal(err)
				}
			case <-quit:
				ticker.Stop()
				return
			}
		}
	}()
	go func() {
		ticker := time.NewTicker(5 * time.Minute)
		for {
			select {
			case <-ticker.C:
				needRestart, err := getFlannelNeedRestart(hosts)
				if err != nil {
					mail.Send("sindre@brurberg.no", "BrurbergOrch Error: restart flannel", err.Error())
					time.Sleep(time.Minute)
					log.Fatal(err)
				}
				if needRestart {
					mail.Send("sindre@brurberg.no", "BrurbergOrch Info: restart flannel", "Restarting flannel.\nIt seems like a server has restarted for a unknown reason.\nIf there is outages for some services this is why.")
					err = restartClusterFlannel(hosts)
					if err != nil {
						mail.Send("sindre@brurberg.no", "BrurbergOrch Error: restart flannel", err.Error())
						return
					}
				}
			}
		}
	}()
	mail.Send("sindre@brurberg.no", "BrurbergOrch Info: Main", "Service starting")

	r := gin.Default()
	dbCon, dbErr := helper.DBConnect{DB_HOST: os.Getenv("POSTGRES_HOST"), DB_USER: os.Getenv("POSTGRES_USER"), DB_PASSWORD: os.Getenv("POSTGRES_PASSWORD"), DB_NAME: os.Getenv("POSTGRES_DB")}.ConnectToDB()
	for dbErr != nil {
		time.Sleep(time.Minute)
		dbCon, dbErr = helper.DBConnect{DB_HOST: os.Getenv("POSTGRES_HOST"), DB_USER: os.Getenv("POSTGRES_USER"), DB_PASSWORD: os.Getenv("POSTGRES_PASSWORD"), DB_NAME: os.Getenv("POSTGRES_DB")}.ConnectToDB()
		if dbErr != nil {
			log.Println(dbErr)
			//	log.Fatal(dbErr)
		}
	}
	db := DB{DB: &helper.DB{Conn: dbCon}}

	api := r.Group("/api")
	api.POST("/update", func(c *gin.Context) {
		var update Update
		err := c.BindJSON(&update)
		if err != nil {
			log.WriteError(err)
			c.JSON(http.StatusUnauthorized, gin.H{"error": err.Error()})
			return
		}
		system, err := db.DB.GetSystem(update.Key)
		if err != nil {
			log.WriteError(err.Error())
			c.JSON(http.StatusUnauthorized, gin.H{"error": "Invalid key"})
			return
		}
		if err := setVersion(system.ConfigPath, system.Container, update.Tag); err != nil { //log.CheckError(err, log.Error) {
			log.Println(err)
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
		if err := applyDeployment(system.ConfigPath); err != nil { //log.CheckError(err, log.Error) {
			log.Println(err)
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
	})

	adm := api.Group("/")
	adm.Use(AdmMiddleware())
	adm.GET("/systems", GetSystems)

	gtw := api.Group("/")
	gtw.Use(GatewayMiddleware())
	gtw.POST("/restartgateway", func(c *gin.Context) {
		restart("gateway", "gateway")
	})

	log.CheckError(r.Run(fmt.Sprintf(":%s", os.Getenv("PORT"))), log.Error)
}

func setVersion(configPath, container, tag string) (err error) {
	log.Println(fmt.Sprintf("configs/%s/deployment.yaml", configPath))
	cmd := exec.Command("sed", "-E", "-i", fmt.Sprintf("s;%[1]s:(\\w|\\.)+;%[1]s:%s;g", container, tag), fmt.Sprintf("configs/%s/deployment.yaml", configPath))
	var out bytes.Buffer
	cmd.Stdout = &out
	log.Println(out.String())
	log.Println(cmd.String())
	err = cmd.Run()
	return
}

func applyDeployment(configPath string) error {
	return apply(configPath, "deployment.yaml")
}

func apply(configPath, file string) (err error) {
	cmd := exec.Command("kubectl", "apply", "-f", fmt.Sprintf("configs/%s/%s", configPath, file))
	var out bytes.Buffer
	cmd.Stdout = &out
	err = cmd.Run()
	return
}

func restart(deployment, namespace string) (err error) {
	cmd := exec.Command("kubectl", "rollout", "restart", fmt.Sprintf("deployments/%s", deployment), "--namespace", namespace)
	err = cmd.Run()
	return
}

func AdmMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		if c.Request.Header.Get("authorization") != "SECRET" { //Static for now
			c.JSON(http.StatusForbidden, gin.H{"error": "Unauthrozed"})
			c.Abort()
			return
		}
		c.Next()
	}
}

func GatewayMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		if c.Request.Header.Get("authorization") != os.Getenv("GATEWAY_SECRET") {
			c.JSON(http.StatusForbidden, gin.H{"error": "Unauthrozed"})
			c.Abort()
			return
		}
		c.Next()
	}
}

type Frontend struct {
	Container string    `json:"container"`
	Version   string    `json:"version"`
	Deployed  time.Time `json:"deployed"`
}

type Backend struct {
	Container string    `json:"container"`
	Version   string    `json:"version"`
	Deployed  time.Time `json:"deployed"`
}

type Postgres struct {
	Container string    `json:"container"`
	Version   string    `json:"version"`
	Deployed  time.Time `json:"deployed"`
}

type Service struct {
	Name      string   `json:"name"`
	ConfiRepo string   `json:"configrepo"`
	Frontend  Frontend `json:"frontend"`
	Backend   Backend  `json:"backend"`
	Postgres  Postgres `json:"postgres"`
}

func GetSystems(c *gin.Context) {
	dir := "configs"
	os.Chdir(dir)

	subDirToSkip := ".git"

	services := make(map[string]interface{})
	fmt.Println("On Unix:")
	err := filepath.Walk(".", func(path string, info os.FileInfo, err error) error {
		if err != nil {
			fmt.Printf("prevent panic by handling failure accessing a path %q: %v\n", path, err)
			return err
		}
		if info.IsDir() {
			switch info.Name() {
			case subDirToSkip:
				fmt.Printf("skipping a dir without errors: %+v \n", info.Name())
				return filepath.SkipDir
			case "frontend":
			case "backend":
			case "postgres":
			case "kubernetes":
				//p := strings.Split(path, "/")
				//name := p[len(p)-2]
				//services[name] = Service{Name: name, ConfiRepo: path}
			}
		} else {
			p := strings.Split(path, "/")
			if len(p) <= 4 {
				fmt.Printf("Skiping file: %q\n", path)
				return nil
			}
			if len(p) == 5 && p[len(p)-1] == "deployment.yaml" {
				//service := services[p[1]]
				var service interface{}
				yamlFile, err := ioutil.ReadFile(path)
				if err != nil {
					fmt.Printf("Error reading YAML file: %s\n", err)
					return err
				}
				err = yaml.Unmarshal(yamlFile, &service)
				if err != nil {
					fmt.Printf("Error parsing YAML file: %s\n", err)
				}
				services[p[1]] = service

				/*container, version := getContainerAndVersion(path)
				switch p[3] {
				case "frontend":
					sercice := services[p[1]]
					sercice.Frontend.Container = container
					sercice.Frontend.Version = version
					sercice.Frontend.Deployed = info.ModTime()
					services[p[1]] = sercice
				case "backend":
					sercice := services[p[1]]
					sercice.Backend.Container = container
					sercice.Backend.Version = version
					sercice.Backend.Deployed = info.ModTime()
					services[p[1]] = sercice
				case "postgres":
					sercice := services[p[1]]
					sercice.Postgres.Container = container
					sercice.Postgres.Version = version
					sercice.Postgres.Deployed = info.ModTime()
					services[p[1]] = sercice
				}*/
			}
		}

		fmt.Printf("visited file or dir: %q\n", path)
		return nil
	})
	if err != nil {
		fmt.Printf("error walking the path %q: %v\n", dir, err)
		c.JSON(http.StatusInternalServerError, gin.H{"error": err})
		return
	}
	c.JSON(http.StatusOK, gin.H{"services": services})
}

func getContainerAndVersion(path string) (string, string) {
	r, _ := regexp.Compile("image: ")

	file, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {

		if r.MatchString(scanner.Text()) {
			//fmt.Println(scanner.Text())
			v := strings.Split(strings.TrimSpace(scanner.Text())[6:], ":")
			return v[0], v[1]
		}

		if err := scanner.Err(); err != nil {
			log.Fatal(err)
		}
	}
	return "", ""
}
