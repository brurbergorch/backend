module gitlab.com/brurbergorch/backend

go 1.15

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/go-ping/ping v0.0.0-20201115131931-3300c582a663
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.2.0
	github.com/tatsushid/go-fastping v0.0.0-20160109021039-d7bb493dee3e
	gitlab.com/brurberg/log v1.0.1
	gitlab.com/brurberglogs/backend v0.0.13
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
