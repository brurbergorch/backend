package main

import (
	"bytes"
	"fmt"
	"log"
	"net"
	"os/exec"
	"strconv"
	"strings"
	"time"

	"github.com/tatsushid/go-fastping"
	"gitlab.com/brurbergorch/backend/mail"
	"gitlab.com/brurbergorch/backend/state"
)

const (
	kubeconfig = "cluster-admin.kubeconfig"
	certPath   = "/var/lib/kubernetes/secrets"
	cert       = "cluster-admin.pem"
	key        = "cluster-admin-key.pem"
	user       = "sindre@"
)

func getKubectlAuth(server string) (err error) {
	err = copyFileFromServer(server, "/etc/kubernetes", kubeconfig)
	if err != nil {
		return
	}
	/*home, err := os.UserHomeDir()
	if err != nil {
		return
	}*/
	err = mvFile(kubeconfig, ".kube/config")
	if err != nil {
		return
	}
	err = copyFileFromServer(server, certPath, cert)
	if err != nil {
		return
	}
	err = mvFile(cert, fmt.Sprintf("%s/%s", certPath, cert))
	if err != nil {
		return
	}
	err = copyFileFromServer(server, certPath, key)
	if err != nil {
		return
	}
	err = mvFile(key, fmt.Sprintf("%s/%s", certPath, key))
	if err != nil {
		return
	}
	return
}

func copyFileFromServer(server, path, file string) (err error) {
	cmd := exec.Command("ssh", fmt.Sprintf("%s%s", user, server), fmt.Sprintf("sudo cp %[1]s/%[2]s ./; chown sindre %[2]s", path, file))
	err = cmd.Run()
	if err != nil {
		return
	}
	cmd = exec.Command("scp", fmt.Sprintf("%s%s:./%s", user, server, file), fmt.Sprintf("/tmp/%s", file))
	err = cmd.Run()
	return
}

func mvFile(fromPath, toPath string) (err error) {
	cmd := exec.Command("sudo", "mv", fmt.Sprintf("/tmp/%s", fromPath), toPath)
	err = cmd.Run()
	return
}

func validateCert(path string) (valid bool, err error) {
	cmd := exec.Command("openssl", "x509", "-enddate", "-noout", "-in", path)
	var out bytes.Buffer
	cmd.Stdout = &out
	err = cmd.Run()
	if err != nil {
		return
	}
	line, err := out.ReadString('\n')
	if err != nil {
		return
	}
	t, err := time.Parse("Jan 2 15:04:05 2006 MST", line[9:len(line)-1])
	if err != nil {
		return
	}
	valid = t.After(time.Now().AddDate(0, 0, 0)) //Does not renew the cert if it has not allready expired
	return
}

func validateServerCert(server, path string) (valid bool, err error) {
	cmd := exec.Command("ssh", fmt.Sprintf("%s%s", user, server), "openssl x509 -enddate -noout -in "+path)
	var out bytes.Buffer
	cmd.Stdout = &out
	err = cmd.Run()
	if err != nil {
		return
	}
	line, err := out.ReadString('\n')
	if err != nil {
		return
	}
	t, err := time.Parse("Jan 2 15:04:05 2006 MST", line[9:len(line)-1])
	if err != nil {
		return
	}
	valid = t.After(time.Now().AddDate(0, 0, 0)) //Does not renew the cert if it has not allready expired
	return
}

func restartCluster(hosts []string) (err error) {
	if state.Debug {
		return
	}
	log.Println("RESTARTING CLUSTER")
	mail.Send("sindre@brurberg.no", "BrurbergOrch Info: Cluster", "Restarting cluster")
	for _, host := range hosts {
		cmd := exec.Command("ssh", fmt.Sprintf("%s%s", user, host), "sleep 30 && sudo reboot &")
		err = cmd.Run()
		if err != nil {
			log.Println("CLUSTER RESTART ERROR: ", err)
			//return
		}
	}
	log.Println("RESTARTING CLUSTER DONE")
	return
}

func clusterRunning(hosts []string) (err error) {
	log.Println("Starting PING CHECK")
	var ra *net.IPAddr
	for _, host := range hosts {
		p := fastping.NewPinger()
		ra, err = net.ResolveIPAddr("ip4:icmp", host)
		if err != nil {
			log.Println(err)
			return
		}
		p.AddIPAddr(ra)
		p.Network("udp")
		alive := false
		p.OnRecv = func(addr *net.IPAddr, rtt time.Duration) {
			log.Printf("IP Addr: %s receive, RTT: %v\n", addr.String(), rtt)
			alive = true
		}
		p.OnIdle = func() {
			log.Println("Ping finished")
		}
		err = p.Run()
		if err != nil {
			log.Println(err)
			return
		}
		if !alive {
			err = fmt.Errorf("Host is down")
			return
		}
	}
	log.Println("PING CHECK END")
	return
}

func restartClusterFlannel(hosts []string) (err error) {
	if state.Debug {
		return
	}
	log.Println("RESTARTING FLANNEL")
	mail.Send("sindre@brurberg.no", "BrurbergOrch Info: Flannel", "Restarting flannel")
	for _, host := range hosts {
		cmd := exec.Command("ssh", fmt.Sprintf("%s%s", user, host), "sudo systemctl restart flannel")
		err = cmd.Run()
		if err != nil {
			return
		}
	}
	log.Println("RESTARTING FLANNEL DONE")
	return
}

func getNewCerts(hosts []string) (err error) {
	valid, _ := validateCert(fmt.Sprintf("%s/%s", certPath, cert))
	if valid {
		return
	}
	valid, _ = validateServerCert(hosts[0], fmt.Sprintf("%s/%s", certPath, cert))
	if !valid {
		restartCluster(hosts) //Not handleing the error from this
		retries := 0
		for clusterRunning(hosts) != nil {
			if retries > 25 {
				err = fmt.Errorf("Cluster did not boot with given time")
				return
			}
			time.Sleep(time.Second * 45)
		}
		err = restartClusterFlannel(hosts)
		if err != nil {
			return
		}
	}
	err = getKubectlAuth(hosts[0])
	return
}

var restartedFlannel bool

func getFlannelNeedRestart(hosts []string) (needRestart bool, err error) {
	if !restartedFlannel {
		restartedFlannel = true
		return true, nil
	}
	for _, host := range hosts {
		var uptime int
		uptime, err = getUptime(host)
		if err != nil {
			return
		}
		if uptime < 5*60 {
			return true, nil
		}
	}
	return
}

func getUptime(host string) (uptime int, err error) {
	cmd := exec.Command("ssh", fmt.Sprintf("%s%s", user, host), fmt.Sprintf("cat /proc/uptime"))
	var stdout bytes.Buffer
	cmd.Stdout = &stdout
	err = cmd.Run()
	if err != nil {
		return
	}
	uptime, err = strconv.Atoi(strings.Split(strings.TrimRight(stdout.String(), "\n"), ".")[0])
	return
}
