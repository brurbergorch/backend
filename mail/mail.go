package mail

import (
	"fmt"
	"net/smtp"
	"os"
)

func Send(address, subject, message string) (err error) {
	// Set up authentication information.
	auth := smtp.PlainAuth(os.Getenv("EMAIL_SENDER"), os.Getenv("EMAIL_USERNAME"), os.Getenv("EMAIL_PASSWORD"), os.Getenv("EMAIL_SERVER"))

	// Connect to the server, authenticate, set the sender and recipient,
	// and send the email all in one step.
	to := []string{address}
	msg := []byte(fmt.Sprintf("From: %s\r\n", os.Getenv("EMAIL_SENDER")) +
		fmt.Sprintf("To: %s\r\n", address) +
		"Subject: " + subject + "\r\n" +
		"\r\n" +
		message +
		"\r\n" +
		" - Brurberg")
	err = smtp.SendMail(os.Getenv("EMAIL_SERVER")+":"+os.Getenv("EMAIL_PORT"), auth, os.Getenv("EMAIL_SENDER"), to, msg)
	if err != nil {
		return
	}
	return
}
