DROP DATABASE IF EXISTS brurbergorch;
CREATE DATABASE brurbergorch;
\c brurbergorch;

CREATE TABLE Systems (
  Key TEXT PRIMARY KEY,
  Repo TEXT NOT NULL,
  ConfigRepo TEXT NOT NULL,
  Container TEXT NOT NULL,
  Domain TEXT NOT NULL,
  ConsentedAt TIMESTAMPTZ DEFAULT Now()
);
