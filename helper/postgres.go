package helper

import (
	"database/sql"
	"fmt"
	"strings"

	_ "github.com/lib/pq"
)

type DBConnect struct {
	DB_HOST     string
	DB_PORT     string
	DB_USER     string
	DB_PASSWORD string
	DB_NAME     string
}

type DB struct {
	Conn *sql.DB
}

type System struct {
	Key        string `josn:"key"`
	Repo       string `json:"repo"`
	ConfigRepo string `json:"configrepo"`
	ConfigPath string `json:"configpath"`
	Container  string `json:"container"`
	Domain     string `json:"domain"`
}

func (dbc DBConnect) ConnectToDB() (*sql.DB, error) {
	dbinfo := ""
	if dbc.DB_PASSWORD == "" {
		dbinfo = fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable",
			dbc.DB_HOST, dbc.DB_USER, dbc.DB_NAME)
	} else {
		//if dbc.DB_PORT == "" {
		dbinfo = fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=disable",
			dbc.DB_HOST, dbc.DB_USER, dbc.DB_PASSWORD, dbc.DB_NAME)
		/*} else {
			dbinfo = fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
				dbc.DB_HOST, dbc.DB_PORT, dbc.DB_USER, dbc.DB_PASSWORD, dbc.DB_NAME)
		}*/
	}

	db, err := sql.Open("postgres", dbinfo)
	if err != nil {
		return nil, err
	}

	return db, db.Ping()
}

func (db DB) GetSystem(key string) (system System, err error) {
	err = db.Conn.QueryRow("SELECT key, repo, configrepo, container, domain FROM systems WHERE key = $1;", key).Scan(&system.Key, &system.Repo, &system.ConfigRepo, &system.Container, &system.Domain)
	if err == nil {
		system.ConfigPath = fmt.Sprintf("%s/%s", system.ConfigRepo, strings.Join(strings.Split(system.Repo, "/")[len(strings.Split(system.ConfigRepo, "/"))-1:], "/"))
	}
	return
}
